import random
import json
'''{
    "PS": 1,
    "LS": [60, 70],
    "P": [10, 10],
    "Custo": 100,
    "Cap": [100, 100],
    "M": 3,
    "LC":   [
                [10, 100],
                [1, 100],
                [100, 5]
            ],
    "N": 2
}'''

'''
 ######Constantes default do problema
    M = 3      #Numero de clientes
    N = 2       #Numero de gateways
    Custo = 100 #custo em MB de cada tarefa
    LS=[60,70]  #latencia do gateway i para o servidor central
    LC = [       #latencia do cliente j para o gateway i
        [10,100],
        [1,100],
        [100,5]
    ]
    PS=1        #Tempo de processamento no servidor central
    P=[10,10]   #Tempo de processamento no gateway i

    Cap=[100,100] #Quantidade de memoria de cada gateway
'''
k=1
for w in range(5):
    for m in range(50):
        for n in range (50):
            k*=10
            t = dict()
            t["M"]=m
            t["N"]=n
            t["Custo"]=10
            t["PS"]=random.randint(1, 10)

            t["Cap"]=[]
            t["P"]=[]
            t["LS"]=[]
            t["LC"]=[]
            for i in range (t["M"]):
                t["LC"].append([])
            for i in range(t["N"]):#para cada gateway
                t["Cap"].append(10*random.randint(1,4))
                t["P"].append(random.randint(10,100))
                t["LS"].append(random.randint(50,100))
                for j in range (t["M"]):
                    t["LC"][j].append(random.randint(1,750))
            print ("gerou %d %d"%(m,n))
            with open('entrada-%d-%d'%(m,n), 'w') as fp:
                json.dump(t, fp)
            print ("salvou %d %d"%(m,n))