#!/bin/bash

for i in {1..49}
do
  for j in {1..49}
    do
        a=$(tail -n 1 saida_comp/saida-$i-$j.heu)
        b=$(tail -n 1 saida_padrao/saida-$i-$j.simplex|tr " " "\n"|head -1)
        if [ "$a" != "$b" ]; then
            echo "dif $i $j \"$a\" \"$b\""
        fi
  done
done