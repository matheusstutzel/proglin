#include <stdio.h>      /* printf */
#include <math.h>       /* fabs */
#include <vector>   

using namespace std;
#define VD vector<int>
#define VVD vector<VD>
#define VI vector<int>
double MinCostMatching(const VVD &cost, VI &Lmate, VI &Rmate);