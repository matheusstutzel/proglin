#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <queue>
#include <utility> 

#include "hungarian.h"

#define INF 0x7fffffff
using namespace std;



typedef  struct {
    int t,g,n;//(tempo,gateway, no)
} item;

int m,n,size;
item *s;//lista com m itens do tipo (gatway, tempo) o mínimo para processar no servidor usando o gateway 
int *aux;
int *c;
item a;

int **mat;

int convert (int w){
    int t=0;
    for(int i =0;i<n;i++){
        if(w<t)return i-1;
        t+=c[i];
    }
}
int main(){
    cin>>m>>n;
    s = (item*)malloc(sizeof(item*)*m);
    aux = (int*)malloc(sizeof(int)*n);
    c = (int*)malloc(sizeof(int)*n);

    size = m;//largura total da matriz, m colunas para os servidores e a soma das capacidades para os gateways
    for(int j = 0; j < n; j++){
        cin>>c[j];
        size+=c[j];
        //cout<<c[j]<<" ";
    }
    //cout<<endl;

    /*mat = (int**)malloc(sizeof(int*)*size);//aloca a matriz
    for(int i=0;i<size;i++)mat[i]=(int*)malloc(sizeof(int)*size);*/

    vector<vector<int> > mat;
    mat.resize(size,std::vector<int>(size, 0));


    for(int i = 0; i < m; i++){//pra cada sensor
        int count=0;
        int y;
        int maior= INF;
        for(int j = 0; j < n; j++){//para cada gateway
            cin>>aux[j]>>y;//le o tempo para processar naquele gateway e o tempo para o servidor passando por esse gateway
            if(y<maior){
                maior=y;
                s[i].t=maior;//armazena apenas o melhor caminho para o servidor 
                s[i].g=j;
            }
        }
        for(int j = 0; j < n; j++){
            y = (aux[j]<s[i].t)?aux[j]:INF;//usando esse gateway é melhor do que o melhor caminho pro servidor?
            for(int k=0;k<c[j];k++)mat[i][count++]=y;//preenche a tabela, a informação desse gateway é repetida o número de vezes da sua capacidade
        }
        y=count+i;
        for(int k=0;k<m;k++)mat[i][count]=((count++==y)?s[i].t:INF);//preenche a parte da matriz relacionada ao melhor caminho para o servidor

    }
    cout<<"[";
    for(int i=0;i<size;i++){
        cout<<"[";
        for(int j=0;j<size;j++)if(mat[i][j]==INF)printf("    ");else printf("%d,",mat[i][j]);
        cout<<"],"<<endl;
    }cout<<"]"<<endl;
    VI xy;
    VI qwe;
    cout<<"min="<< MinCostMatching(mat , xy,qwe) <<endl;
    int w,k,v;
    double z=0;
    for(int i = 0; i < m; i++){//pra cada sensor
        w=xy[i];
        //while(hungarian.assignment()[i][++w]==0);
        cout<<w<<endl;
        if(w<size-m){
            k=convert(w);
        }else{
            k=s[i].g;
        }
        //v = (mat[i][w]/10000.0f)*s[i].t;
        v = mat[i][w];
        printf("Cliente %02d - latência: %05d, conectado ao gateway: %02d, utilizando o %s\n",i+1,v,k+1,((w<size-m)?"gateway":"servidor"));
        z=(v>z)?v:z;
    }
    printf("%.1f\n",z);
    return 0;
}