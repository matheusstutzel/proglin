#include <iostream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <queue>
#include <utility> 

#define INF 0x7fffffff
using namespace std;

typedef  struct {
    int t,g,n;//(tempo,gateway, no)
} item;

class mycomparison{
  bool reverse;
public:
  mycomparison(const bool& revparam=false)
    {reverse=revparam;}
  bool operator() (const item& lhs, const item&rhs) const
  {
      /*if(&lhs==NULL && &rhs ==NULL)return 0;
      if(&lhs==NULL)return 1;
      if(&rhs==NULL)return -1;*/
    if (reverse) return (lhs.t>rhs.t);
    else return (lhs.t<rhs.t);
  }
};
typedef priority_queue< item,vector<item>,mycomparison > fila; 

int m,n,size;
item s[60];//lista com m itens do tipo (gatway, tempo) o mínimo para processar no servidor usando o gateway 
fila q;//lista com itens do tipo (tempo,gateway, no)
int aux[60];
int c[60];
item a;
int **mat;

int main(){
    cin>>m>>n;
    
    size = m;
    for(int j = 0; j < n; j++){
        cin>>c[j];
        size+=c[j];
    }
    mat = (int**)malloc(sizeof(int*)*m);
    for(int i=0;i<size;i++)mat[i]=(int*)malloc(sizeof(int)*size);

    for(int i = 0; i < m; i++){
        int count=0;
        int y;
        int maior= INF;
        for(int j = 0; j < n; j++){
            cin>>aux[j]>>y;
            if(y<maior){
                maior=y;
                s[i].t=maior;
                s[i].g=j;
            }
        }
        for(int j = 0; j < n; j++){
            y = (aux[j]<s[i].t)?aux[j]:INF;
            for(int k=0;k<c[j];k++)mat[i][count++]=y;
        }
        y=count+i;
        for(int k=0;k<m;k++)mat[i][k]=(k==y)?s[i].t:INF;
    }
  
    
    memset(aux,0,sizeof(aux));
    while(!q.empty()){
        a = q.top();
        q.pop();
        cout<<a.g<<" "<<a.n<<" "<<a.t<<" s"<<s[a.n].t<<endl;
        if(aux[a.n]!=0)continue;//já foi e
        if(c[a.g]>0){
            c[a.g];
        }
        
    }
    return 0;
}