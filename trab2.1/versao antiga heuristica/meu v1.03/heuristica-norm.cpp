#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <queue>
#include <utility> 

#include "hungarian.h"

#define INF 0x7fffffff
using namespace std;

typedef  struct {
    int t,g,n;//(tempo,gateway, no)
} item;

int m,n,size;
item s[60];//lista com m itens do tipo (gatway, tempo) o mínimo para processar no servidor usando o gateway 
int aux[60];
int c[60];
item a;
int **mat;

void imprimeMat(){
    for(int i=0;i<m;i++){
        for(int j=0;j<size;j++)printf("%010d ",mat[i][j]);
        cout<<endl;
    }
}
int convert (int w){
    int t=0;
    for(int i =0;i<n;i++){
        if(w<t)return i-1;
        t+=c[i];
    }
}
int main(){
    cin>>m>>n;
    
    size = m;//largura total da matriz, m colunas para os servidores e a soma das capacidades para os gateways
    for(int j = 0; j < n; j++){
        cin>>c[j];
        size+=c[j];
        //cout<<c[j]<<" ";
    }
    //cout<<endl;
    mat = (int**)malloc(sizeof(int*)*m);//aloca a matriz
    for(int i=0;i<m;i++)mat[i]=(int*)malloc(sizeof(int)*size);

    for(int i = 0; i < m; i++){//pra cada sensor
        int count=0;
        int y;
        int maior= INF;
        for(int j = 0; j < n; j++){//para cada gateway
            cin>>aux[j]>>y;//le o tempo para processar naquele gateway e o tempo para o servidor passando por esse gateway
            if(y<maior){
                maior=y;
                s[i].t=maior;//armazena apenas o melhor caminho para o servidor 
                s[i].g=j;
            }
        }
        for(int j = 0; j < n; j++){
            y = (aux[j]<s[i].t)?(int)(aux[j]/((s[i].t*1.0f))*10000):INF;//usando esse gateway é melhor do que o melhor caminho pro servidor?
            for(int k=0;k<c[j];k++)mat[i][count++]=y;//preenche a tabela, a informação desse gateway é repetida o número de vezes da sua capacidade
        }
        y=count+i;
        for(int k=0;k<m;k++)mat[i][count]=((count++==y)?10000:INF);//preenche a parte da matriz relacionada ao melhor caminho para o servidor

    }
    //imprimeMat();
    

    hungarian_problem_t p;
    int matrix_size = hungarian_init(&p, mat , m,size, HUNGARIAN_MODE_MINIMIZE_COST) ;

    //fprintf(stderr, "assignement matrix has a now a size %d rows and %d columns.\n\n",  matrix_size,matrix_size);

    //fprintf(stderr, "cost-matrix:");
    //hungarian_print_costmatrix(&p);
    hungarian_solve(&p);
    //fprintf(stderr, "assignment:");
    //hungarian_print_assignment(&p);
    int w,k,v;
    double z=0;
    for(int i = 0; i < m; i++){//pra cada sensor
        w=-1;
        while(p.assignment[i][++w]==0);
        //cout<<w<<endl;
        if(w<size-m){
            k=convert(w);
        }else{
            k=s[i].g;
        }
        v = (mat[i][w]/10000.0f)*s[i].t;
        printf("Cliente %02d - latência: %05d, conectado ao gateway: %02d, utilizando o %s\n",i+1,v,k+1,((w<size-m)?"gateway":"servidor"));
        z=(v>z)?v:z;
    }
    printf("%.1f\n",z);
    hungarian_free(&p);
    return 0;
}