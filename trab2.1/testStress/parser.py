import numpy as np
import scipy as sp
import scipy.stats

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-h, m+h


i=0;
data =[]
with open("testeStress.data", 'r') as s:
    with open("testeStress.parser", 'w') as d:
        for lines in s:
            if(i==0):
                data=[]
            i+=1
            (num,v)=lines.split(" ");
            data.append(float(v))
            if(i==30):
                a,b,c = mean_confidence_interval(data)
                #d.write(str(num)+" "+mean_confidence_interval(data))
                d.write("{}, {}, {}, {}\n".format(num,a,b,c))
                i=0


