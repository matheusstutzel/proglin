reset
set encoding utf8
fontsize = 18
set key font ",20"
set format y '10^{%L}'
set format x '10^{%L}'
set logscale y
set term postscript enhanced eps color
set output "testStress.eps"
set style fill solid 1.00 border 0
set style histogram errorbars gap 2 lw 1
set style data histograms
set xtics rotate by -45
set title "Análise de desempenho da Heurística" font ",20"
set xlabel "Gateways e Clientes" font ",20"
set ylabel "Tempo (s)" font ",20"
set bars 0.8
#set yrange [0 : 40]
set datafile separator ","
plot 'testeStress.parser' \
		   using 2:3:4:xtic(1) ti "Heurística" linecolor rgb "#FF0000"
