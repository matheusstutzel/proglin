reset
set encoding utf8
set term postscript enhanced eps color 
set output "comp2.eps"
set key off


set title "Comparação do desempenho GLPK" font ",20"
set xlabel "Clientes" font ",20"
set ylabel "Gateways" font ",20"


#m,n 

set pm3d map
set zrange [1.01 : 31]
set palette defined (0 "green", 1 "blue",2 "red", 31 "black")
splot 'comp.parse' using 1:2:3 with points palette pointsize .9 pointtype 7