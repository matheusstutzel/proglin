reset
set encoding utf8
set term postscript enhanced eps color 
set output "simu1.eps"
set key off



set title "Desempenho do GLPK" font ",20" offset 0,-5
set xlabel "Clientes" font ",20" 
set ylabel "Gateways" font ",20"
set zlabel "Passos" font ",20" rotate by 90 center


#set zrange [1 : 31]
splot 'results1.parse' using 1:2:3 with points palette pointsize .66 pointtype 7
