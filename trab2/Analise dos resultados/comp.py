
m = [[0 for x in range(51)] for y in range(51)] 
with open('results1.parse', 'r') as fp:
    for lines in fp:
        a = lines.split('\t')
        m[int(a[0])][int(a[1])]=int(a[2])

with open('results2.parse', 'r') as fp:
    for lines in fp:
        a = lines.split('\t')
        m[int(a[0])][int(a[1])]=float(a[2])/m[int(a[0])][int(a[1])]

with open('comp.parse', 'w') as fp:
    for i in range(1,50):
        for j in range(1,50):
            #if(m[i][j]!=1):
            fp.write("%d %d %f\n"%(i,j,m[i][j]))
            