#!/usr/bin/python
# -*- coding: utf-8-*-
from glpk.glpkpi import *
import json
import sys

if(len(sys.argv)>1):
    with open(sys.argv[1], 'r') as myfile:
        data=myfile.read().replace('\n', '')
    j = json.loads(data)
    M = j['M']
    N = j['N']
    Custo = j['Custo']
    LS = j['LS']
    LC = j['LC']
    PS = j['PS']
    P = j['P']
    Cap = j['Cap']
else:
    ######Constantes default do problema
    M = 3      #Numero de clientes
    N = 2       #Numero de gateways
    Custo = 100 #custo em MB de cada tarefa
    LS=[60,70]  #latencia do gateway i para o servidor central
    LC = [       #latencia do cliente j para o gateway i
        [10,100],
        [1,100],
        [100,5]
    ]
    PS=1        #Tempo de processamento no servidor central
    P=[10,10]   #Tempo de processamento no gateway i

    Cap=[100,100] #Quantidade de memoria de cada gateway

LCinvalido = False
LCinvalido = len(LC)!=M
i=0
while not LCinvalido and i<M:
    LCinvalido=len(LC[i])!=N
    i+=1
if(len(LS)!=N or len(P)!=N or len(Cap)!=N or LCinvalido):
    print "Dados invalidos"
    exit()

size = 100000+1
ia = intArray(size)
ja = intArray(size)
ar = doubleArray(size)
prob = glp_create_prob()
glp_set_prob_name(prob, "process-location")

glp_set_obj_dir(prob,GLP_MIN)

index=1                     #variável auxiliar para contagem de variáveis
glp_add_cols(prob, 1)       #criando a variavel d
glp_set_col_name(prob, index, "d")
glp_set_col_bnds(prob, index, GLP_LO, 0.0, 0.0)
glp_set_obj_coef(prob, index, 1.0)


glp_add_cols(prob, M)           #criando as variaveis X_i
for i in range(1,M+1):
   index+=1
   glp_set_col_name(prob, index, "x"+str(i))
   glp_set_col_bnds(prob, index, GLP_LO, 0.0, 0.0)
   glp_set_obj_coef(prob, index, 0.0)

glp_add_cols(prob,M*N*2)        #criando as variaveis Y_ijk
for k in range(1,3):
    for j in range(1,M+1): 
        for i in range(1,N+1):       
            index+=1
            glp_set_col_name(prob, index, "Y("+str(i)+","+str(j)+","+str(k)+")")
            glp_set_col_kind(prob,index,GLP_BV)
            glp_set_obj_coef(prob, index, 0.0)

c=0
line=0
for i in range(M):          #para cada uma das M restrições da equação 4 (d-xj>=P  para todo j in M)
    line+=1
    c+=1
    #d deve ser multiplicada por 1
    ia[c]=line
    ja[c]=1
    ar[c]=1
    c+=1
    #Xi deve ser multiplicado por -1
    ia[c]=line
    ja[c]=i+2
    ar[c]=-1

for j in range(M):          #para cada uma das M restrições da equação 5 (xj - SUM(Ayij1 + Byij2) = 0)
    line+=1

    #Xi deve ser multiplicado por 1
    c+=1
    ia[c]=line
    ja[c]=j+2
    ar[c]=1

    offset_Yij1 = (M+1+(j*N))+1
    offset_Yij2 = (M+1+M*N+(j*N))+1
    for i in range(N):
        #yij1 deve ser multiplicado por -A
        #A=Pi + LCij
        c+=1
        ia[c]=line
        ja[c]=i+offset_Yij1
        ar[c]=-(P[i]+LC[j][i])

        #yij2 deve ser multiplicado por -B
        #B=LCij + LSi + PS
        c+=1
        ia[c]=line
        ja[c]=i+offset_Yij2
        ar[c]=-(LC[j][i]+LS[i]+PS)


for j in range(M):          #criando as M restrições da equação 6 (SUM(yij1 + yij2) = 1)
    line+=1
    offset_Yij1 = (M+1+(j*N))+1
    offset_Yij2 = (M+1+M*N+(j*N))+1
    for i in range(N):
        #yij1 deve ser multiplicado por 1
        c+=1
        ia[c]=line
        ja[c]=i+offset_Yij1
        ar[c]=1

        #yij2 deve ser multiplicado por 1
        c+=1
        ia[c]=line
        ja[c]=i+offset_Yij2
        ar[c]=1

for i in range(N):          #criando as N restrições da equação 7 (SUM(Capi*yij1) <= Capi)
    line+=1
    offset_Yij1 = (M+1+i)+1
    for j in range(M):
        #yij1 deve ser multiplicado por Custo
        c+=1
        ia[c]=line
        ja[c]=(j*N)+offset_Yij1
        ar[c]=Custo


line=0
glp_add_rows(prob,M)        #criando as M restrições da equação 4 (d<=xj  para todo j in M)
for i in range(M):
    line+=1
    glp_set_row_name(prob, line, "equation4-"+str(i))
    glp_set_row_bnds(prob, line, GLP_LO, 0.0, 0.0)

glp_add_rows(prob,M)        #criando as M restrições da equação 5 (xj - SUM(Ayij1 + Byij2) = 0)
for i in range(M):
    line+=1
    glp_set_row_name(prob, line, "equation5-"+str(i+1))
    glp_set_row_bnds(prob, line, GLP_FX, 0.0, 0.0)

glp_add_rows(prob,M)        #criando as M restrições da equação 6 (SUM(yij1 + yij2) = 1)
for i in range(M):
    line+=1
    glp_set_row_name(prob, line, "equation6-"+str(i+1))
    glp_set_row_bnds(prob, line, GLP_FX, 1.0, 1.0)

glp_add_rows(prob,N)        #criando as N restrições da equação 7 (SUM(C*yij1 )<= Capi)
for i in range(N):
    line+=1
    glp_set_row_name(prob, line, "equation7-"+str(i+1))
    glp_set_row_bnds(prob, line, GLP_UP, 0.0, Cap[i])
    
glp_load_matrix(prob,c, ia, ja, ar)
glp_write_lp(prob,None,"process-location.lp")
glp_simplex(prob, None)
glp_intopt(prob, None)

Z = glp_mip_obj_val(prob)
print "Menor maior lat:",Z
index=1  
c=[]
for i in range(M):
   index+=1
   c.append("Cliente %02d - latência: %02d"%((i+1),glp_mip_col_val(prob, index)))

for k in range(2):
    for j in range(M): 
        for i in range(N):       
            index+=1
            if(glp_mip_col_val(prob, index)>0):
                c[j]+=", conectado ao gateway: %02d, utilizando o %s"%(i+1, ("gateway"if k+1==1 else"servidor"))
for w in c:
    print w
del prob

